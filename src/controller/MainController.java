package controller;

import game.Audio;
import view.Platform;
import model.*;
import model.Block;
import model.Coin;
import model.Tunnel;
import utils.Res;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lisamazzini on 12/03/17.
 */
public class MainController {

    private static final int MARIO_X = 300;
    private static final int MARIO_Y = 245;
    private static final int TURTLE_X = 800;
    private static final int TURTLE_Y = 243;
    private static final int MUSHROOM_X = 950;
    private static final int MUSHROOM_Y = 263;

    private EnemyController mushroomController;
    private EnemyController turtleController;
    private MarioController marioController;
    private List<GameElement> objects;
    private List<Coin> coins;
    private Platform platform;

    public MainController(Platform platform) {

        this.marioController = new MarioController(new Mario(MARIO_X, MARIO_Y), platform);
        this.objects = new ArrayList<>();
        this.coins = new ArrayList<>();
        EnemyFactory factory = new EnemyFactory();
        this.turtleController = factory.createTurtle(TURTLE_X, TURTLE_Y).getEnemyController();
        this.mushroomController = factory.createMushroom(MUSHROOM_X, MUSHROOM_Y).getEnemyController();
        this.platform = platform;

        this.initialiseEnvironment();

    }

    public void refreshEnvironment() {

        this.checkNearbyObjects();

        this.checkNearbyCoins();

        this.checkNearbyCharacters();

        this.makeEnemiesMove();
    }

    public MarioController getMarioController(){
        return this.marioController;
    }

    public List<Enemy> getEnemies(){
        List<Enemy> enemies = new ArrayList<>();
        enemies.add(mushroomController.getEnemy());
        enemies.add(turtleController.getEnemy());

        return enemies;
    }

    public List<GameElement> getObjects() {
        return objects;
    }

    public List<Coin> getCoins() {
        return coins;
    }

    private void initialiseEnvironment(){
        this.objects.add(new Tunnel(600, 230));
        this.objects.add(new Tunnel(1000, 230));
        this.objects.add(new Tunnel(1600, 230));
        this.objects.add(new Tunnel(1900, 230));
        this.objects.add(new Tunnel(2500, 230));
        this.objects.add(new Tunnel(3000, 230));
        this.objects.add(new Tunnel(3800, 230));
        this.objects.add(new Tunnel(4500, 230));

        this.objects.add(new Block(400, 180));
        this.objects.add(new Block(1200, 180));
        this.objects.add(new Block(1270, 170));
        this.objects.add(new Block(1340, 160));
        this.objects.add(new Block(2000, 180));
        this.objects.add(new Block(2600, 160));
        this.objects.add(new Block(2650, 180));
        this.objects.add(new Block(3500, 160));
        this.objects.add(new Block(3550, 140));
        this.objects.add(new Block(4000, 170));
        this.objects.add(new Block(4200, 200));
        this.objects.add(new Block(4300, 210));

        this.coins.add(new Coin(402, 145));
        this.coins.add(new Coin(1202, 140));
        this.coins.add(new Coin(1272, 95));
        this.coins.add(new Coin(1342, 40));
        this.coins.add(new Coin(1650, 145));
        this.coins.add(new Coin(2650, 145));
        this.coins.add(new Coin(3000, 135));
        this.coins.add(new Coin(3400, 125));
        this.coins.add(new Coin(4200, 145));
        this.coins.add(new Coin(4600, 40));
    }

    private void checkNearbyObjects(){
        for(GameElement gameElement : this.objects){

            if (this.marioController.isNearby(gameElement)) {
                this.marioController.contact(gameElement);
            }

            if (this.mushroomController.isNearby(gameElement)) {
                this.mushroomController.contact(gameElement);
            }

            if (this.turtleController.isNearby(gameElement)) {
                this.turtleController.contact(gameElement);
            }

            this.move(gameElement);
        }
    }

    private void checkNearbyCoins(){
        for (int i = 0; i < coins.size(); i++) {
            if (this.marioController.contactPiece(this.coins.get(i))) {
                Audio.playSound(Res.AUDIO_MONEY);
                this.coins.remove(i);
            }
            this.move(this.coins.get(i));
        }
    }

    private void checkNearbyCharacters(){
        if (this.mushroomController.isNearby(turtleController.getEnemy())) {
            this.mushroomController.contact(turtleController.getEnemy());
        }
        if (this.turtleController.isNearby(mushroomController.getEnemy())) {
            this.turtleController.contact(mushroomController.getEnemy());
        }
        if (this.marioController.isNearby(mushroomController.getEnemy())) {
            this.marioController.contact(mushroomController.getEnemy());
        }
        if (this.marioController.isNearby(turtleController.getEnemy())) {
            this.marioController.contact(turtleController.getEnemy());
        }
    }

    private void makeEnemiesMove(){
        this.mushroomController.move();
        this.turtleController.move();
    }

    private void move(GameElement gameElement){
        if (this.platform.getxPos() >= 0) {
            gameElement.setX(gameElement.getX() - this.platform.getMov());
        }
    }
}
