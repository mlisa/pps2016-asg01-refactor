package controller;

import model.Enemy;
import model.GameElement;

public class EnemyController implements CharacterController {

    private Enemy enemy;
    private static final int MARGIN = 5;


    public EnemyController(Enemy enemy) {
        this.enemy = enemy;
    }

    public Enemy getEnemy() {
        return enemy;
    }

    public void move() {
        this.enemy.setOffsetX(this.enemy.isMovingToRight() ? 1 : -1);
        this.enemy.setX(this.enemy.getX() + this.enemy.getOffsetX());
    }

    public boolean isNearby(GameElement element) {
        return ((this.enemy.getX() > element.getX() - Enemy.PROXIMITY_MARGIN && this.enemy.getX() < element.getX() + element.getWidth() + Enemy.PROXIMITY_MARGIN)
                || (this.enemy.getX() + this.enemy.getWidth() > element.getX() - Enemy.PROXIMITY_MARGIN && this.enemy.getX() + this.enemy.getWidth() < element.getX() + element.getWidth() + Enemy.PROXIMITY_MARGIN));
    }

    public void contact(GameElement gameElement) {
        this.checkIfContactObject(gameElement);
    }

    public boolean checkIfAlive(){
        return this.enemy.isAlive();
    }

    private boolean hitAhead(GameElement element) {
        return !(this.enemy.getX() + this.enemy.getWidth() < element.getX() || this.enemy.getX() + this.enemy.getWidth() > element.getX() + MARGIN ||
                this.enemy.getY() + this.enemy.getHeight() <= element.getY() || this.enemy.getY() >= element.getY() + element.getHeight());
    }

    private boolean hitBack(GameElement element) {
        return !(this.enemy.getX() > element.getX() + element.getWidth() || this.enemy.getX() + this.enemy.getWidth() < element.getX() + element.getWidth() - MARGIN ||
                this.enemy.getY() + this.enemy.getHeight() <= element.getY() || this.enemy.getY() >= element.getY() + element.getHeight());
    }

    private void checkIfContactObject(GameElement element){
        if (this.hitAhead(element) && this.enemy.isMovingToRight()) {
            this.enemy.setIsMovingToRight(false);
            this.enemy.setOffsetX(-1);
        } else if (this.hitBack(element) && !this.enemy.isMovingToRight()) {
            this.enemy.setIsMovingToRight(true);
            this.enemy.setOffsetX(1);
        }
    }
}
