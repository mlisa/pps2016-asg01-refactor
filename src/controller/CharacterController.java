package controller;

import model.GameElement;

/**
 * Created by lisamazzini on 14/03/17.
 */
public interface CharacterController {

    boolean isNearby(GameElement pers);

    void contact(GameElement gameElement);

    boolean checkIfAlive();
}
