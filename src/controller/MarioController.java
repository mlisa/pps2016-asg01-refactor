package controller;

import view.Platform;
import model.Character;
import model.Coin;
import model.GameElement;
import model.Mario;
import utils.Res;

/**
 * Created by lisamazzini on 12/03/17.
 */
public class MarioController implements CharacterController {

    private static final int MARGIN = 5;

    private Mario mario;
    private String jumpingImage;
    private Platform platform;

    public MarioController(Mario mario, Platform platform) {
        this.mario = mario;
        this.platform = platform;
    }

    public int getMarioX(){
        return this.mario.getX();
    }

    public int getMarioY(){
        return this.mario.getY();
    }

    public String getMarioWalkingImage(String name, int frequency){
        return this.mario.getWalkingImage(name, frequency);
    }

    public void setMarioMoving(boolean moving){
        this.mario.setIsMoving(moving);
    }

    public void setMarioMovingRight(boolean movingToRight){
        this.mario.setIsMovingToRight(movingToRight);
    }

    public void setMarioJumping(boolean jumping){
        this.mario.setJumping(jumping);
    }

    public boolean isNearby(GameElement element) {
        return (this.mario.getX() > element.getX() - Mario.PROXIMITY_MARGIN && this.mario.getX() < element.getX() + element.getWidth() + Mario.PROXIMITY_MARGIN)
                || (this.mario.getX() + this.mario.getWidth() > element.getX() - Mario.PROXIMITY_MARGIN &&
                this.mario.getX() + this.mario.getWidth() < element.getX() + element.getWidth() + Mario.PROXIMITY_MARGIN);
    }

    public void contact(Character character) {
        this.makeMarioContactCharacter(character);
    }

    public void contact(GameElement gameElement) {
        this.makeMarioContactObject(gameElement);
    }

    public boolean contactPiece(Coin coin) {
        return this.hitBack(coin) || this.hitAbove(coin) || this.hitAhead(coin)
                || this.hitBelow(coin);
    }

    public String getMarioJumpingImage() {
        this.makeMarioJump();
        return this.jumpingImage;
    }

    public boolean isMarioJumping(){
        return this.mario.isJumping();
    }

    public boolean checkIfAlive(){
        return this.mario.isAlive();
    }

    private boolean hitAbove(GameElement element) {
        return !(this.mario.getX() + this.mario.getWidth() < element.getX() + MARGIN || this.mario.getX() > element.getX() + element.getWidth() - MARGIN ||
                this.mario.getY() < element.getY() + element.getHeight() || this.mario.getY() > element.getY() + element.getHeight() + MARGIN);

    }

    private boolean hitAhead(GameElement element) {
        return  !(this.mario.getX() + this.mario.getWidth() < element.getX() || this.mario.getX() + this.mario.getWidth() > element.getX() + MARGIN ||
                this.mario.getY() + this.mario.getHeight() <= element.getY() || this.mario.getY() >= element.getY() + element.getHeight());
    }

    private boolean hitBack(GameElement element) {
        return !(this.mario.getX() > element.getX() + element.getWidth() || this.mario.getX() + this.mario.getWidth() < element.getX() + element.getWidth() - MARGIN ||
                this.mario.getY() + this.mario.getHeight() <= element.getY() || this.mario.getY() >= element.getY() + element.getHeight());
    }

    private boolean hitBelow(GameElement element) {
        return !(this.mario.getX() + this.mario.getWidth() < element.getX() || this.mario.getX() > element.getX() + element.getWidth() ||
                this.mario.getY() + this.mario.getHeight() < element.getY() || this.mario.getY() + this.mario.getHeight() > element.getY());
    }

    private void makeMarioJump(){
        this.mario.setJumpingExtent(this.mario.getJumpingExtent() + 1 );
        if (this.mario.getJumpingExtent() < Mario.JUMPING_LIMIT) {
            if (this.mario.getY() > this.platform.getHeightLimit()) {
                this.mario.setY(this.mario.getY() - 4);
            } else {
                this.mario.setJumpingExtent(Mario.JUMPING_LIMIT);
            }
            this.jumpingImage = this.mario.isMovingToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;

        } else if (this.mario.getY() + this.mario.getHeight() < this.platform.getFloorOffsetY()) {
            this.mario.setY(this.mario.getY() + 1);
            this.jumpingImage = this.mario.isMovingToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            this.jumpingImage = this.mario.isMovingToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.mario.setJumping(false);
            this.mario.setJumpingExtent(0);
        }
    }

    private void makeMarioContactObject(GameElement gameElement){
        if (this.hitAhead(gameElement) && this.mario.isMovingToRight() || this.hitBack(gameElement) && !this.mario.isMovingToRight()) {
            this.platform.setMov(0);
            this.mario.setIsMoving(false);
        }

        if (this.hitBelow(gameElement) && this.mario.isJumping()) {
            this.platform.setFloorOffsetY(gameElement.getY());
        } else if (!this.hitBelow(gameElement)) {
            this.platform.setFloorOffsetY(Mario.FLOOR_OFFSET_Y_INITIAL);
            if (!this.mario.isJumping()) {
                this.mario.setY(Mario.MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(gameElement)) {
                this.platform.setHeightLimit(gameElement.getY() + gameElement.getHeight()); // the new sky goes below the object
            } else if (!this.hitAbove(gameElement) && !this.mario.isJumping()) {
                this.platform.setHeightLimit(0); // initial sky
            }
        }
    }

    private void makeMarioContactCharacter(Character character){
        if (this.hitAhead(character) || this.hitBack(character)) {
            if (character.isAlive()) {
                this.mario.setIsMoving(false);
                this.mario.setAlive(false);
            } else {
                this.mario.setAlive(true);
            }
        } else if (this.hitBelow(character)) {
            character.setIsMoving(false);
            character.setAlive(false);
        }
    }
}
