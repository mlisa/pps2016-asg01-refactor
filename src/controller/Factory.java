package controller;

/**
 * Created by lisamazzini on 13/03/17.
 */
public interface Factory {

    EnemyThread createTurtle(int x, int y);

    EnemyThread createMushroom(int x, int y);

}
