package controller;

import model.Enemy;
import model.ElementType;

/**
 * Created by lisamazzini on 13/03/17.
 */
public class EnemyFactory implements Factory {

    @Override
    public EnemyThread createTurtle(int x, int y) {
        return new EnemyThread(new EnemyController(new Enemy(x, y, ElementType.TURTLE)));
    }

    @Override
    public EnemyThread createMushroom(int x, int y) {
        return new EnemyThread(new EnemyController(new Enemy(x, y, ElementType.MUSHROOM)));
    }
}
