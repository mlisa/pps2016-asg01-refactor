package controller;

import utils.Res;

/**
 * Created by lisamazzini on 12/03/17.
 */
public class EnemyThread implements Runnable {

    private EnemyController enemyController;

    public EnemyThread(EnemyController enemyController) {
        this.enemyController = enemyController;

        Thread enemyThread = new Thread(this);
        enemyThread.start();
    }

    public EnemyController getEnemyController() {
        return enemyController;
    }

    @Override
    public void run() {
        while (true) {
            if (this.enemyController.checkIfAlive()) {
                this.enemyController.move();
                try {
                    Thread.sleep(Res.PAUSE);
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
