package game;

import controller.MarioController;
import view.Platform;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    private MarioController marioController;
    private Platform platform;

    public Keyboard(Platform platform) {
        this.platform = platform;
        this.marioController = platform.getMainController().getMarioController();
    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (this.marioController.checkIfAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                if (this.platform.getxPos() == -1) {
                    this.platform.setxPos(0);
                    this.platform.setBackground1PosX(-50);
                    this.platform.setBackground2PosX(750);
                }
                this.marioController.setMarioMoving(true);
                this.marioController.setMarioMovingRight(true);
                this.platform.setMov(1);
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (this.platform.getxPos() == 4601) {
                    this.platform.setxPos(4600);
                    this.platform.setBackground1PosX(-50);
                    this.platform.setBackground2PosX(750);
                }

                this.marioController.setMarioMoving(true);
                this.marioController.setMarioMovingRight(false);
                this.platform.setMov(-1);
            }

            if (e.getKeyCode() == KeyEvent.VK_UP) {
                this.marioController.setMarioJumping(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        this.marioController.setMarioMoving(false);
        this.platform.setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
