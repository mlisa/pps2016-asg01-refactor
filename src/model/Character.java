package model;

public interface Character extends GameElement{

	boolean isAlive();

	boolean isMovingToRight();

	void setAlive(boolean alive);

	void setIsMoving(boolean moving);

	void setIsMovingToRight(boolean toRight);

	String getWalkingImage(String name, int frequency);
}
