package model;

import utils.Res;

/**
 * Created by lisamazzini on 12/03/17.
 */
public class Enemy extends BasicCharacter {

    private int offsetX;
    private ElementType type;

    public Enemy(int x, int y, ElementType type) {
        super(x, y, type.getWidth(), type.getHeight(), type.getDefaultImage());
        this.setIsMovingToRight(true);
        this.setIsMoving(true);
        this.offsetX = 1;
        this.type = type;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public void setOffsetX(int offsetX) {
        this.offsetX = offsetX;
    }

    public ElementType getType(){
        return this.type;
    }

    public String getDeadImage() {
        switch (this.type){
            case MUSHROOM:
                return this.isMovingToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX;
            case TURTLE:
                return Res.IMG_TURTLE_DEAD;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
