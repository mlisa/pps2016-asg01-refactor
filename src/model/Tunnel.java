package model;

public class Tunnel extends GameObject {

    public Tunnel(int x, int y) {
        super(x, y, ElementType.TUNNEL.getWidth(), ElementType.TUNNEL.getHeight());
        super.imgObj = ElementType.TUNNEL.getDefaultImage();
    }

}
