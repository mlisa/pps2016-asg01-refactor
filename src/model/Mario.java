package model;

public class Mario extends BasicCharacter {

    public static final int MARIO_OFFSET_Y_INITIAL = 243;
    public static final int FLOOR_OFFSET_Y_INITIAL = 293;
    public static final int JUMPING_LIMIT = 42;

    private boolean jumping;
    private int jumpingExtent;

    public Mario(int x, int y) {
        super(x, y, ElementType.MARIO.getWidth(), ElementType.MARIO.getHeight(), ElementType.MARIO.getDefaultImage());
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public int getJumpingExtent() {
        return jumpingExtent;
    }

    public void setJumpingExtent(int jumpingExtent) {
        this.jumpingExtent = jumpingExtent;
    }

}
