package model;

public class Block extends GameObject {

    public Block(int x, int y) {
        super(x, y, ElementType.BLOCK.getWidth(), ElementType.BLOCK.getHeight());
        super.imgObj = ElementType.BLOCK.getDefaultImage();
    }

}
