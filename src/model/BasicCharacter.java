package model;

import utils.Res;

public class BasicCharacter implements Character {

    public static final int PROXIMITY_MARGIN = 10;
    private int width, height;
    private int x, y;
    private boolean moving;
    private boolean toRight;
    private int counter;
    private boolean alive;
    private String image;

    public BasicCharacter(int x, int y, int width, int height, String image) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
        this.image = image;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getObjectImage(){
        return this.image;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isMovingToRight() {
        return toRight;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setIsMoving(boolean moving) {
        this.moving = moving;
    }

    public void setIsMovingToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public String getWalkingImage(String name, int frequency) {
        return  Res.IMG_BASE + name + (!this.moving || ++this.counter % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
    }

}
