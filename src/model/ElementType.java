package model;

import utils.Res;

import java.awt.*;

/**
 * Created by lisamazzini on 12/03/17.
 */
public enum ElementType {

    MUSHROOM (27, 30, Res.IMG_MUSHROOM_DEFAULT),
    TURTLE (43, 50, Res.IMG_TURTLE_IDLE),
    MARIO(28, 50, Res.IMG_MARIO_DEFAULT),
    BLOCK(30, 30, Res.IMG_BLOCK),
    TUNNEL(43, 65, Res.IMG_TUNNEL),
    COIN(30, 30, Res.IMG_PIECE1);

    private int width;
    private int height;
    private String defaultImage;


    ElementType(int width, int height, String defaultImage) {
        this.width = width;
        this.height = height;
        this.defaultImage = defaultImage;
    }

    public int getWidth(){
        return this.width;
    }

    public int getHeight() {
        return height;
    }

    public String getDefaultImage() {
        return defaultImage;
    }
}
