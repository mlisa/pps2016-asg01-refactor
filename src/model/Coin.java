package model;

import utils.Res;

public class Coin extends GameObject implements Runnable {

    private static final int PAUSE = 10;
    private static final int FLIP_FREQUENCY = 50;
    private int counter;

    public Coin(int x, int y) {
        super(x, y, ElementType.COIN.getWidth(), ElementType.COIN.getHeight());
        super.imgObj = ElementType.COIN.getDefaultImage();
    }

    public String imageOnMovement() {
        return (++this.counter % FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

}
