package model;

/**
 * Created by lisamazzini on 12/03/17.
 */
public interface GameElement {

    int getX();

    int getY();

    int getWidth();

    int getHeight();

    String getObjectImage();

    void setX(int x);

    void setY(int y);

}
